// Sample Go code for user authorization

package main

import (
	"encoding/json"
	"fmt"
	"google.golang.org/api/youtube/v3"
	"log"
)

func handleError(err error, message string) {
	if message == "" {
		message = "Error making API call"
	}
	if err != nil {
		log.Fatalf(message+": %v", err.Error())
	}
}

func channelsListByUsername(service *youtube.Service, part string, forUsername string) {
	call := service.Channels.List([]string{part})
	call = call.Id(forUsername)
	response, err := call.Do()
	handleError(err, "")
	data, err := json.Marshal(response.Items)
	fmt.Println(string(data))
}

//func main() {
//ctx := context.Background()

//b, err := ioutil.ReadFile("client_secret.json")
//if err != nil {
//	log.Fatalf("Unable to read client secret file: %v", err)
//}

// If modifying these scopes, delete your previously saved credentials
// at ~/.credentials/youtube-go-quickstart.json
//config, err := google.ConfigFromJSON(b, youtube.YoutubeReadonlyScope)
//if err != nil {
//	log.Fatalf("Unable to parse client secret file to config: %v", err)
//}
//	client := getClient(youtube.YoutubeReadonlyScope)
//	service, err := youtube.New(client)

//	handleError(err, "Error creating YouTube client")

//	channelsListByUsername(service, "snippet,contentDetails,statistics", "UCmbs8T6MWqUHP1tIQvSgKrg")
//}
