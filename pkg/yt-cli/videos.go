package yt_cli

import (
	"encoding/json"
	"fmt"
	"google.golang.org/api/youtube/v3"
)

func GetVideoById(service *youtube.Service, part string, id string) {
	call := service.Videos.List([]string{part})
	call = call.Id(id)
	call = call.MaxResults(50)
	handleVideoListCall(call)
}

func handleVideoListCall(call *youtube.VideosListCall) {
	response, err := call.Do()
	handleError(err, "")
	var Items []*youtube.Video
	Items = append(Items, response.Items...)
	NextPageToken := response.NextPageToken
	println(NextPageToken)
	for i := 0; i < 50; i++ {
		if NextPageToken != "" {
			call = call.PageToken(NextPageToken)
			subResponse, err := call.Do()
			handleError(err, "")
			Items = append(Items, subResponse.Items...)
			NextPageToken = subResponse.NextPageToken
			println("--------------")
		} else {
			break
		}
	}
	data, err := json.Marshal(Items)
	fmt.Println(string(data))
}
