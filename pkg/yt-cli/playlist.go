package yt_cli

import (
	"encoding/json"
	"fmt"
	"google.golang.org/api/youtube/v3"
)

func GetPlaylistByChannelId(service *youtube.Service, part string, id string) {
	call := service.Playlists.List([]string{part})
	call = call.ChannelId(id)
	response, err := call.Do()
	handleError(err, "")
	data, err := json.Marshal(response.Items)
	fmt.Println(string(data))
}

func GetPlaylistByPlaylistId(service *youtube.Service, part string, id string) {
	call := service.PlaylistItems.List([]string{part})
	call = call.PlaylistId(id)
	call = call.MaxResults(50)
	handlePlaylistItemsListCall(call, service, part, id)
}

func GetPlaylistByPlayListItemId(service *youtube.Service, part string, id string) {
	call := service.PlaylistItems.List([]string{part})
	call = call.Id(id)
	call = call.MaxResults(50)
	handlePlaylistItemsListCall(call, service, part, id)
}

func handlePlaylistItemsListCall(call *youtube.PlaylistItemsListCall, service *youtube.Service, part string, id string) {
	response, err := call.Do()
	handleError(err, "")
	var Items []*youtube.PlaylistItem
	Items = append(Items, response.Items...)
	NextPageToken := response.NextPageToken
	for i := 0; i < 50; i++ {
		if NextPageToken != "" {
			call = call.PageToken(NextPageToken)
			subResponse, err := call.Do()
			handleError(err, "")
			Items = append(Items, subResponse.Items...)
			NextPageToken = subResponse.NextPageToken
		} else {
			break
		}
	}
	data, err := json.Marshal(Items)
	fmt.Println(string(data))
}
