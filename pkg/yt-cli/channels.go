package yt_cli

import (
	"encoding/json"
	"fmt"
	"google.golang.org/api/youtube/v3"
	"log"
)

func handleError(err error, message string) {
	if message == "" {
		message = "Error making API call"
	}
	if err != nil {
		log.Fatalf(message+": %v", err.Error())
	}
}

func ListChannelsByUsername(service *youtube.Service, part string, forUsername string) {
	call := service.Channels.List([]string{part})
	call = call.ForUsername(forUsername)
	handleChannelsListCall(call)
}

func GetChannelById(service *youtube.Service, part string, id string) {
	call := service.Channels.List([]string{part})
	call = call.Id(id)
	handleChannelsListCall(call)
}

func handleChannelsListCall(call *youtube.ChannelsListCall) {
	response, err := call.Do()
	handleError(err, "")
	data, err := json.Marshal(response.Items)
	fmt.Println(string(data))
}
