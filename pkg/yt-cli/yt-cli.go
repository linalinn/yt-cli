package yt_cli

import (
	"google.golang.org/api/youtube/v3"
	"net/http"
)

func GetService(client *http.Client) (*youtube.Service, error) {
	return youtube.New(client)
}
