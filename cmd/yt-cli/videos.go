package yt_cli

import (
	"fmt"
	"github.com/spf13/cobra"
	yt_cli "gitlab.com/linalinn/yt-cli/pkg/yt-cli"
)

var videosCmd = &cobra.Command{
	Use:   "videos",
	Short: "interact with videos",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("use list or get sub command")
	},
}

var getVideoCmd = &cobra.Command{
	Use:   "get",
	Short: "get a video by Id",
	Run: func(cmd *cobra.Command, args []string) {
		client := yt_cli.GetReadOnlyClient()
		service, _ := yt_cli.GetService(client)
		yt_cli.GetVideoById(service, "player,localizations,recordingDetails,statistics,liveStreamingDetails,status,snippet,contentDetails,topicDetails", getVal(cmd.Flags().GetString("video-id")))
	},
}

func init() {
	getVideoCmd.Flags().String("video-id", "", "find channel by id")
	getVideoCmd.MarkFlagRequired("video-id")
	videosCmd.AddCommand(getVideoCmd)
	rootCmd.AddCommand(videosCmd)
}
