package yt_cli

import (
	"fmt"
	"github.com/spf13/cobra"
	yt_cli "gitlab.com/linalinn/yt-cli/pkg/yt-cli"
)

var authCmd = &cobra.Command{
	Use:   "auth",
	Short: "Login to google account",
	Run: func(cmd *cobra.Command, args []string) {
		renew, err := cmd.Flags().GetBool("renew")
		if err != nil {
			panic("Flag error in auth")
		}
		res := yt_cli.Auth(renew)
		fmt.Println(res)
	},
}

func init() {
	authCmd.PersistentFlags().Bool("renew", false, "renew login")
	rootCmd.AddCommand(authCmd)
}
