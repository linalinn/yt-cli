package yt_cli

import (
	"fmt"
	"github.com/spf13/cobra"
	yt_cli "gitlab.com/linalinn/yt-cli/pkg/yt-cli"
)

var channelsCmd = &cobra.Command{
	Use:   "channels",
	Short: "list and update channels",
	Run: func(cmd *cobra.Command, args []string) {
		renew, err := cmd.Flags().GetBool("renew")
		if err != nil {
			panic("Flag error in auth")
		}
		res := yt_cli.Auth(renew)
		fmt.Println(res)
	},
}

func err[T any, U error](_ T, err U) U {
	return err
}

func getVal[T any, U error](val T, _ U) T {
	return val
}

var channelsListCmd = &cobra.Command{
	Use:   "list",
	Short: "lists channels that match the search criteria",
	Long:  `lists channels that match the search criteria at lest one is required`,
	Run: func(cmd *cobra.Command, args []string) {
		client := yt_cli.GetReadOnlyClient()
		service, _ := yt_cli.GetService(client)
		switch true {
		case cmd.Flags().Changed("username"):
			yt_cli.ListChannelsByUsername(service, "snippet,contentDetails,statistics", getVal(cmd.Flags().GetString("username")))
		case cmd.Flags().Changed("channel-id"):
			yt_cli.GetChannelById(service, "snippet,contentDetails,statistics", getVal(cmd.Flags().GetString("channel-id")))
		default:
			fmt.Println("you need at lest one filter")
		}
	},
}

func init() {
	channelsListCmd.Flags().String("username", "", "find channels by username")
	channelsListCmd.Flags().String("channel-id", "", "find channel by id")
	channelsListCmd.MarkFlagsMutuallyExclusive("username", "channel-id")
	channelsCmd.AddCommand(channelsListCmd)
	rootCmd.AddCommand(channelsCmd)
}
