package yt_cli

import (
	"fmt"
	"github.com/spf13/cobra"
	yt_cli "gitlab.com/linalinn/yt-cli/pkg/yt-cli"
)

var playlitsCmd = &cobra.Command{
	Use:   "playlist",
	Short: "interact with playlists",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("use list or get sub command")
	},
}

var getPlaylitsCmd = &cobra.Command{
	Use:   "get",
	Short: "get Content of an playlist",
	Run: func(cmd *cobra.Command, args []string) {
		client := yt_cli.GetReadOnlyClient()
		service, _ := yt_cli.GetService(client)
		switch true {
		case cmd.Flags().Changed("playlist-id"):
			yt_cli.GetPlaylistByPlaylistId(service, "snippet,contentDetails", getVal(cmd.Flags().GetString("playlist-id")))
		case cmd.Flags().Changed("playlist-item-id"):
			yt_cli.GetPlaylistByPlayListItemId(service, "snippet,contentDetails", getVal(cmd.Flags().GetString("playlist-item-id")))
		default:
			fmt.Println("you need at lest one filter")
		}
	},
}

var listPlaylitsCmd = &cobra.Command{
	Use:   "list",
	Short: "lists channels playlists",
	Run: func(cmd *cobra.Command, args []string) {
		client := yt_cli.GetReadOnlyClient()
		service, _ := yt_cli.GetService(client)
		yt_cli.GetPlaylistByChannelId(service, "snippet,contentDetails,statistics", getVal(cmd.Flags().GetString("channel-id")))
	},
}

func init() {
	getPlaylitsCmd.Flags().String("playlist-id", "", "get playlist by playlistId")
	getPlaylitsCmd.Flags().String("playlist-item-id", "", "get playlist by playlistItemId")
	getPlaylitsCmd.MarkFlagsMutuallyExclusive("playlist-id", "playlist-item-id")
	playlitsCmd.AddCommand(getPlaylitsCmd)
	playlitsCmd.AddCommand(listPlaylitsCmd)
	rootCmd.AddCommand(playlitsCmd)
}
